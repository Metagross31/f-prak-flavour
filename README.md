# F-Prak-Flavour TMNF/TMNU server

## How to suggest addition of maps

The easiest/fastest to deploy way of adding maps to the map pool is via a Merge Request.

The map's `.Challenge.Gbx` file has to be added to an appropriate directory within `GameData/Tracks/Challenges/`.
Ideally put it either in `GameData/Tracks/Challenges/Downloaded/` or create/look for a directory for a specific map author, such as `GameData/Tracks/Challenges/Downloaded/Lurk93/`.

Additionally, add the path to the newly added map file to `GameData/Tracks/MatchSettings/Nations/MetasMaps.txt`.
The paths can contain pretty much any ASCII character, not sure about UTF-8.
So rather be safe than sorry and don't use too wild characters in the file/path names.
Check out the file to see in what format maps are usually added and just follow it.

If you do not want to bother with creating a Merge Request, just add an issue requesting to add the map, including the maps name and ideally its TMX link (e.g. https://tmnf.exchange/trackshow/9904888).
If I do not see your issue/MR, just send me a message on WhatsApp/Telegram/Email whatever and I will take a look at it.

